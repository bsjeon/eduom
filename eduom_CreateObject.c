/******************************************************************************/
/*                                                                            */
/*    ODYSSEUS/EduCOSMOS Educational-Purpose Object Storage System            */
/*                                                                            */
/*    Developed by Professor Kyu-Young Whang et al.                           */
/*                                                                            */
/*    Database and Multimedia Laboratory                                      */
/*                                                                            */
/*    Computer Science Department and                                         */
/*    Advanced Information Technology Research Center (AITrc)                 */
/*    Korea Advanced Institute of Science and Technology (KAIST)              */
/*                                                                            */
/*    e-mail: kywhang@cs.kaist.ac.kr                                          */
/*    phone: +82-42-350-7722                                                  */
/*    fax: +82-42-350-8380                                                    */
/*                                                                            */
/*    Copyright (c) 1995-2013 by Kyu-Young Whang                              */
/*                                                                            */
/*    All rights reserved. No part of this software may be reproduced,        */
/*    stored in a retrieval system, or transmitted, in any form or by any     */
/*    means, electronic, mechanical, photocopying, recording, or otherwise,   */
/*    without prior written permission of the copyright owner.                */
/*                                                                            */
/******************************************************************************/
/*
 * Module : eduom_CreateObject.c
 * 
 * Description :
 *  eduom_CreateObject() creates a new object near the specified object.
 *
 * Exports:
 *  Four eduom_CreateObject(ObjectID*, ObjectID*, ObjectHdr*, Four, char*, ObjectID*)
 */


#include <string.h>
#include "EduOM_common.h"
#include "RDsM.h"		/* for the raw disk manager call */
#include "BfM.h"		/* for the buffer manager call */
#include "EduOM_Internal.h"



/*@================================
 * eduom_CreateObject()
 *================================*/
/*
 * Function: Four eduom_CreateObject(ObjectID*, ObjectID*, ObjectHdr*, Four, char*, ObjectID*)
 * 
 * Description :
 * (Following description is for original ODYSSEUS/COSMOS OM.
 *  For ODYSSEUS/EduCOSMOS EduOM, refer to the EduOM project manual.)
 *
 *  eduom_CreateObject() creates a new object near the specified object; the near
 *  page is the page holding the near object.
 *  If there is no room in the near page and the near object 'nearObj' is not
 *  NULL, a new page is allocated for object creation (In this case, the newly
 *  allocated page is inserted after the near page in the list of pages
 *  consiting in the file).
 *  If there is no room in the near page and the near object 'nearObj' is NULL,
 *  it trys to create a new object in the page in the available space list. If
 *  fail, then the new object will be put into the newly allocated page(In this
 *  case, the newly allocated page is appended at the tail of the list of pages
 *  cosisting in the file).
 *
 * Returns:
 *  error Code
 *    eBADCATALOGOBJECT_OM
 *    eBADOBJECTID_OM
 *    some errors caused by fuction calls
 */
Four eduom_CreateObject(
    ObjectID	*catObjForFile,	/* IN file in which object is to be placed */
    ObjectID 	*nearObj,	/* IN create the new object near this object */
    ObjectHdr	*objHdr,	/* IN from which tag & properties are set */
    Four	length,		/* IN amount of data */
    char	*data,		/* IN the initial data for the object */
    ObjectID	*oid)		/* OUT the object's ObjectID */
{
    Four        e;		/* error number */
    Four	neededSpace;	/* space needed to put new object [+ header] */
    SlottedPage *apage;		/* pointer to the slotted page buffer */
    Four        alignedLen;	/* aligned length of initial data */
    Boolean     needToAllocPage;/* Is there a need to alloc a new page? */
    PageID      pid;            /* PageID in which new object to be inserted */
    PageID      nearPid;
    Four        firstExt;	/* first Extent No of the file */
    Object      *obj;		/* point to the newly created object */
    Two         i;		/* index variable */
    sm_CatOverlayForData *catEntry; /* pointer to data file catalog information */
    SlottedPage *catPage;	/* pointer to buffer containing the catalog */
    FileID      fid;		/* ID of file where the new object is placed */
    Two         eff;		/* extent fill factor of file */
    Boolean     isTmp;
    PhysicalFileID pFid;
    

    /*@ parameter checking */
    
    if (catObjForFile == NULL) ERR(eBADCATALOGOBJECT_OM);

    if (objHdr == NULL) ERR(eBADOBJECTID_OM);
    
    /* Error check whether using not supported functionality by EduOM */
    if(ALIGNED_LENGTH(length) > LRGOBJ_THRESHOLD) ERR(eNOTSUPPORTED_EDUOM);

	/*
	 * JBS Code
	 */

	//Calculate needed space
	alignedLen = ALIGNED_LENGTH(length);
	neededSpace = sizeof(ObjectHdr) + sizeof(SlottedPageSlot) + alignedLen;
	e = BfM_GetTrain((TrainID*)catObjForFile, (char**)&catPage, PAGE_BUF);
	if (e<0) ERR(e);
	GET_PTR_TO_CATENTRY_FOR_DATA(catObjForFile, catPage, catEntry);

	// Set Related Variables
	needToAllocPage = FALSE;
	pid.volNo = catEntry->fid.volNo;	

	// Select page that object will be added to
	if (nearObj != NULL) {
		nearPid.volNo = nearObj->volNo;
		nearPid.pageNo = nearObj->pageNo;

		e = BfM_GetTrain((TrainID*)nearObj, (char**)&apage, PAGE_BUF);
		if (e<0) ERR(e);
		// Case : There is enough free space
		if (SP_FREE(apage) >= neededSpace) {
			pid.pageNo = nearObj->pageNo;
			pid.volNo = nearObj->volNo;
		} else {
			needToAllocPage = TRUE;			
			e = BfM_FreeTrain((TrainID*)nearObj, PAGE_BUF);
			if (e<0) ERR(e);
		}			
	} else {
		pid.pageNo = -1;
		// I'm not sure that -1 means no List
		if (neededSpace < SP_20SIZE && catEntry->availSpaceList10 != -1) {
			pid.pageNo = catEntry->availSpaceList10;	
		} else if (neededSpace < SP_30SIZE && catEntry->availSpaceList20 != -1) {
			pid.pageNo = catEntry->availSpaceList20;
		} else if (neededSpace < SP_40SIZE && catEntry->availSpaceList30 != -1) {
			pid.pageNo = catEntry->availSpaceList30;
		} else if (neededSpace < SP_50SIZE && catEntry->availSpaceList40 != -1) {
			pid.pageNo = catEntry->availSpaceList40;
		} else if (neededSpace >= SP_50SIZE && catEntry->availSpaceList50 != -1) {
			pid.pageNo = catEntry->availSpaceList50;
		}
		
		if (pid.pageNo == -1) {
			pid.pageNo = catEntry->lastPage;
			e = BfM_GetTrain((TrainID*)&pid, (char **)&apage, PAGE_BUF);
			if (e<0) ERR(e);

			if (SP_FREE(apage) < neededSpace) {
				nearPid.volNo = pid.volNo;
				nearPid.pageNo = pid.pageNo;
				
				needToAllocPage = TRUE;	
		
				e = BfM_FreeTrain((TrainID*)&pid,  PAGE_BUF);
				if (e<0) ERR(e);
			}
		} else {
			e = BfM_GetTrain((TrainID*)&pid, (char **)&apage, PAGE_BUF);
			if (e<0) ERR(e);
		}
	}

	// Alloc PAGE
	if (needToAllocPage == TRUE) {
		pid.pageNo = catEntry->firstPage;
		
		// Requirement : First Page's pid.
		e = RDsM_PageIdToExtNo(&pid, &firstExt);
		if (e<0) ERR(e);
		e = RDsM_AllocTrains(catEntry->fid.volNo, firstExt, &nearPid, catEntry->eff, 1, 1, &pid);
		if (e<0) ERR(e);
		// What is exact diff btw GetNewTrain & GetTrain?
		e = BfM_GetNewTrain(&pid, (char**)&apage, PAGE_BUF);
		if (e<0) ERR(e);
		
		// Initialize Header (need to fix)
		apage->header.pid = pid;
		apage->header.flags = SLOTTED_PAGE_TYPE;
		apage->header.reserved = 0;
		apage->header.nSlots = 0;
		apage->header.free = 0;
		apage->header.unused = 0;
		apage->header.fid = catEntry->fid;
		
		apage->header.unique = 0;
		apage->header.uniqueLimit = 0;
		apage->header.nextPage=-1;
		apage->header.prevPage=-1;
		apage->header.spaceListPrev=-1;
		apage->header.spaceListNext=-1;
		e = om_FileMapAddPage(catEntry, &nearPid, &pid);
		if (e<0) ERR(e);
	} else {
		e = om_RemoveFromAvailSpaceList(catObjForFile, &pid, apage);
		if (e<0) ERR(e);	
	}

	// Compact PAGE
	if (apage->header.unused > 0) {
		if (e = EduOM_CompactPage(apage,NIL) < 0) ERR(e);
	}

	// Add Object to selected page.
	objHdr->length = length;
	Four l_offset = apage->header.free;
	memcpy(apage->data+l_offset, objHdr, sizeof(ObjectHdr));
	memcpy(apage->data+l_offset+sizeof(ObjectHdr), data, length);

	SlotNo l_selectedSlot = -1;
	Two l_slottedPageSlotSiz = 0;
	for (i=0; i>=-apage->header.nSlots+1; --i) {
		if (apage->slot[i].offset == EMPTYSLOT) {
			l_selectedSlot = -i;
			break;
		}
	}
	
	if (l_selectedSlot == -1) {
		apage->header.nSlots++;
		l_selectedSlot = apage->header.nSlots-1;
		l_slottedPageSlotSiz = sizeof(SlottedPageSlot);
	}

	// Set Slot information
	apage->slot[-l_selectedSlot].offset = l_offset;
	e = om_GetUnique(&pid, &apage->slot[-l_selectedSlot].unique);
	if (e<0) ERR(e);

	// set Page Information	
	apage->header.free += sizeof(ObjectHdr)+alignedLen; 
	if (e = om_PutInAvailSpaceList(catObjForFile, &pid, apage) < 0) ERR(e);

	// Set Object ID
	oid->volNo = pid.volNo;
	oid->pageNo = pid.pageNo;
	oid->slotNo = l_selectedSlot;
	oid->unique = apage->slot[-l_selectedSlot].unique;

	// Free Used Trains
	if (e = BfM_SetDirty(&pid, PAGE_BUF) < 0) ERR(e);
	e = BfM_FreeTrain((TrainID*)catObjForFile, PAGE_BUF);
	if (e<0) ERR(e);
	
	e = BfM_FreeTrain(&pid, PAGE_BUF);
	if (e<0) ERR(e);

    return(eNOERROR);
    
} /* eduom_CreateObject() */
